package com.appspot.magtech.observables;

public interface Outputable {

    void output(String str);
}
