package com.appspot.magtech.observables

import com.appspot.magtech.observables.delegates.ObservableVar
import com.appspot.magtech.observables.extensions.addListener
import com.appspot.magtech.observables.extensions.bind
import com.appspot.magtech.observables.extensions.event
import com.appspot.magtech.observables.utils.r
import com.appspot.magtech.observables.utils.resolveBy
import com.appspot.magtech.observables.utils.resolveByName
import com.appspot.magtech.observables.utils.resolveByType
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class ResolversTest {

    interface Model {
        var amount: Int
        var total: Double
        var list: ArrayList<String>
    }

    class ModelImpl: Model {
        override var amount by ObservableVar(0)
        override var total by ObservableVar(0.0)
        override var list by ObservableVar(arrayListOf<String>())
    }

    @Test
    fun testListenersWithResolveByName() {
        val model: Model = ModelImpl()
        var changedValue = 0
        model.amount = changedValue
        resolveByName<Int>(model, "amount").addListener {
            changedValue = it
        }
        model.amount = 5
        assertEquals(5, changedValue)
    }

    @Test
    fun testListenersWithResolveByType() {
        val model: Model = ModelImpl()
        var changedAmountValue = 0
        model.amount = changedAmountValue
        resolveByType(model, Int::class).addListener {
            changedAmountValue = it
        }
        model.amount = 5
        assertEquals(5, changedAmountValue)

        var changedListValue = arrayListOf<String>()
        model.list = changedListValue
        resolveByType<ArrayList<String>>(model).addListener {
            changedListValue = it
        }
        model.list = arrayListOf("abc", "def")
        assertEquals(arrayListOf("abc", "def"), changedListValue)
    }

    @Test
    fun testListenersWithResolveByProp() {
        val model: Model = ModelImpl()
        var changedValue = 0
        model.amount = changedValue
        (model::amount resolveBy model).addListener {
            changedValue = it
        }
        model.amount = 5
        assertEquals(5, changedValue)
    }

    @Test
    fun testListenersWithRFun() {
        val model: Model = ModelImpl()
        var changedValue = 0
        model.amount = changedValue
        r(model, { ::amount }).addListener {
            changedValue = it
        }
        model.amount = 5
        assertEquals(5, changedValue)
    }

    @Test
    fun testBindingWithResolveByName() {
        val model: Model = ModelImpl()
        class Entity {
            var amount: Int = 0
            var total: Double = 0.0
        }
        val entity = Entity()

        model.amount = 3
        entity::amount.bind(resolveByName(model, "amount"))
        assertEquals(3, entity.amount)
        model.amount = 5
        assertEquals(5, entity.amount)

        model.total = 3.0
        entity::total.bind(resolveByName(model, "total"))
        assertEquals(3.0, entity.total)
        model.total = 10.0
        assertEquals(10.0, entity.total)
    }

    @Test
    fun testEventWithResolveByName() {
        val model: Model = ModelImpl()
        var flag = false
        class Entity {
            fun action() {
                flag = true
            }
        }
        val entity = Entity()
        entity::action.event(resolveByName<Int>(model, "amount"))
        model.amount = 10
        assertEquals(true, flag)
    }
}