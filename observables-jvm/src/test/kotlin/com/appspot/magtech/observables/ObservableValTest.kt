package com.appspot.magtech.observables

import com.appspot.magtech.observables.delegates.ObservableVar
import com.appspot.magtech.observables.extensions.Listener
import com.appspot.magtech.observables.extensions.UnsupportedDelegateException
import com.appspot.magtech.observables.extensions.addListener
import com.appspot.magtech.observables.extensions.getListeners
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class ObservableValTest {

    class TestEntity {

        var amount by ObservableVar(0)

        var title by ObservableVar<String?>(null)

        var percent: Double = 0.0
    }

    @Test
    @DisplayName("Should add listeners to ObservableVar properties")
    fun addListenersTest() {
        val entity = TestEntity()
        var changedValue = entity.title
        entity::title.addListener {
            changedValue = it
        }

        val entity2 = TestEntity()
        var changedValue2 = entity2.title
        entity2::title.addListener {
            changedValue2 = it
        }

        entity.title = "abc"
        entity2.title = "def"
        assertEquals("abc", changedValue)
        assertEquals("def", changedValue2)
    }

    @Test
    @DisplayName("should get listeners from ObservableVar property")
    fun getListenersTest() {
        val entity = TestEntity()
        entity::amount.addListener {
            println("First Listener: $it")
        }
        entity::amount.addListener {
            println("Second Listener: $it")
        }
        entity::amount.getListeners().forEach {
            assertEquals(
                "com.appspot.magtech.observables.extensions.Observer<kotlin.Int>.(kotlin.Int) -> kotlin.Unit",
                it.toString()
            )
        }
        println(entity.amount)
    }

    @Test
    fun removeListenersTest() {
        val entity = TestEntity()
        var changedValue = entity.amount
        val listener: Listener<Int> = { newVal: Int ->
            changedValue = newVal
        }
        entity::amount.addListener(listener)
        entity.amount = 5
        entity::amount.getListeners().remove(listener)
        entity.amount = 10
        assertEquals(5, changedValue)
    }

    @Test
    @DisplayName("should throw exception if not an ObservableVar field")
    fun wrongPropsTest() {
        val entity = TestEntity()
        assertThrows<UnsupportedDelegateException> {
            entity::percent.addListener {
                println("New value: $it")
            }
        }
        assertThrows<UnsupportedDelegateException> {
            entity::percent.getListeners()
        }
    }

    @Test
    fun observersTest() {
        val entity = TestEntity()

        entity::amount.addListener {
            assertEquals(0, prevValue)
            assertEquals(5, it)
        }
        entity::title.addListener {
            assertEquals("abc", it)
        }

        entity.amount = 5
        entity.title = "abc"
    }
}