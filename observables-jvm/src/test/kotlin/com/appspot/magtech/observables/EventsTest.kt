package com.appspot.magtech.observables

import com.appspot.magtech.observables.delegates.ObservableVar
import com.appspot.magtech.observables.extensions.event
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class EventsTest {

    class Model {

        var amount by ObservableVar(0)

        var title by ObservableVar("")
    }
    private val model = Model()

    @Test
    fun testEventFun() {

        var playFlag = 0
        var nextActionFlag = 0
        class Target {

            fun play() {
                playFlag++
            }
            fun nextAction() {
                nextActionFlag++
            }
        }
        val target = Target()

        target::play.event(model::amount)
        model.amount = 5
        assertEquals(1, playFlag)

        target::nextAction.event(model::title) { it.isNotEmpty() }
        model.title = "abc"
        model.title = ""
        assertEquals(1, nextActionFlag)
    }

    @Test
    fun testEventFunOnPlatformClasses() {
        var outputResult: String? = ""
        val outputableImpl = { str: String? ->
            outputResult = str
        }
        val test = TestClass(outputableImpl)

        test::action.event(model::title)
        model.title = "abc"
        assertEquals("Platform action", outputResult)
    }
}