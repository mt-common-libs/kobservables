package com.appspot.magtech.observables.extensions

import com.appspot.magtech.observables.delegates.ObservableVar
import kotlin.reflect.KMutableProperty0
import kotlin.reflect.jvm.isAccessible

@Suppress("UNCHECKED_CAST")
fun <T> KMutableProperty0<T>.addListener (listener: Listener<T>) {
    getObservableVarDelegate().listeners += listener
}

fun <T> KMutableProperty0<T>.getListeners(): MutableList<Listener<T>> =
    getObservableVarDelegate().listeners



class UnsupportedDelegateException(err: String): Exception(err)

@Suppress("UNCHECKED_CAST")
fun <T> KMutableProperty0<T>.getObservableVarDelegate(): ObservableVar<T> {
    isAccessible = true
    val delegate = getDelegate()
    isAccessible = false
    if (delegate == null || delegate !is ObservableVar<*>) {
        throw UnsupportedDelegateException("Property ${this.name} is not an ObservableVar")
    } else {
        return delegate as ObservableVar<T>
    }
}
