package com.appspot.magtech.observables.extensions

import kotlin.reflect.KMutableProperty
import kotlin.reflect.KMutableProperty0
import kotlin.reflect.full.memberFunctions

fun <T> KMutableProperty<T>.bind(target: KMutableProperty0<T>) = this.bind(target) {it}

fun <F, T> KMutableProperty<F>.bind(target: KMutableProperty0<T>, fromTarget: (T) -> F) {
    setter.call(fromTarget(target.get()))
    val delegate = target.getObservableVarDelegate()
    delegate.listeners += {
        setter.call(fromTarget(it))
    }
}

fun <T> Any.bind(vararg propName: String, target: KMutableProperty0<T>) =
    bind(*propName, target = target) { newVal: T -> newVal }

fun <T> Any.bind(propName: String, target: KMutableProperty0<T>) =
    bindSingle(propName, target) { newVal: T -> newVal }



fun <F, T> Any.bind(vararg propName: String, target: KMutableProperty0<T>, fromTarget: (T) -> F) {
    propName.forEach { singleProp ->
        bindSingle(singleProp, target, fromTarget)
    }
}

fun <F, T> Any.bind(propName: String, target: KMutableProperty0<T>, fromTarget: (T) -> F) {
    bindSingle(propName, target, fromTarget)
}

private fun <F, T> Any.bindSingle(propName: String, target: KMutableProperty0<T>, fromTarget: (T) -> F) {
    val setterName = "set${propName.mapIndexed { i, c ->
        if (i == 0) {
            c.toUpperCase()
        } else c
    }.joinToString("")}"
    val prop = this::class.memberFunctions.find {
        it.name == setterName
    } ?: throw PropertyNotFoundException(setterName)

    prop.call(this, fromTarget(target.get()))
    val delegate = target.getObservableVarDelegate()
    delegate.listeners += {
        prop.call(this@bindSingle, fromTarget(it))
    }
}

class PropertyNotFoundException(propName: String): Exception("Property $propName is not found")