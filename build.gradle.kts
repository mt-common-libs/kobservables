val kotlinVersion = "1.3.50"

buildscript {
    val kotlinVersion = "1.3.50"
    
    repositories {
        mavenCentral()
        google()
    }
    dependencies {
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlinVersion")
    }
}

plugins {
    `java-library`
}

apply(plugin = "kotlin-platform-common")

allprojects {
    repositories {
        google()
        jcenter()
    }
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib-common:$kotlinVersion")

    testImplementation("org.jetbrains.kotlin:kotlin-test-annotations-common:$kotlinVersion")
    testImplementation("org.jetbrains.kotlin:kotlin-test-common:$kotlinVersion")
}