package com.appspot.magtech.observables.extensions

data class Observer<T>(val prevValue: T)

typealias Listener<T> = Observer<T>.(T) -> Unit